# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 16:44:04 2017

@author: hossa
"""

from scipy.integrate import odeint
import matplotlib.pyplot as plt
##import ODEs_CA3
import numpy as np
##

def solve_ODEs_CA3():
    
    def ODEs_CA3(X,V):
     
   
        E1R=4000
        E2R=9000
        dHRx1A=-20000
        dHRx2A=-60000
        CpA=90
        CpB=90
        CpC=180
        Ua=4000
        Ta=373
        T0=423
        CT0=0.1
        
        [T,FA,FB,FC]=X
        
        FT=FA+FB+FC
        CA=CT0*(FA/FT)*(T0/T) 
        CB=CT0*(FB/FT)*(T0/T) 
        CC=CT0*(FC/FT)*(T0/T)

        E1R=4000 #K 
        E2R=9000 #K  

        k1A=10*np.exp(E1R*((1/300)-(1/T))) 
        k2A=0.09*np.exp(E2R*(1/300-1/T)) 

        r1A=-k1A*CA #reaction 1 
        r2A=-k2A*(CA)**2 #reaction 2 
         
        dHRx1A=-20000 # J/mol
        dHRx2A=-60000 # J/mol 
         
        CpA=90 #J/mol.C 
        CpB=90 #J/mol.C 
        CpC=180 #J/mol.C 
         
        Ua= 4000 #J/m3.s.C 
        Ta=323 #k(CONSTT.) 
        #rate laws
        rA=r1A+r2A  
        r1B=-r1A 
        rB=r1B
        r2C=-(1/2)*r2A
        rC=r2C 
        #mole balance
        dFAdV=rA 
        dFBdV=rB 
        dFCdV=rC 
        #energy balance
        dTdV=(Ua*(Ta-T)+(-r1A)*(-dHRx1A)+(-r2A)*(-dHRx2A))/(FA*CpA+FB*CpB+FC*CpC) 
         
        F=np.array([dTdV,dFAdV,dFBdV,dFCdV])

        return F


##def solve_ODEs_CA3():
    t=np.arange(0,1,.01)
    y0=np.array([423,100,0,0])
    y=odeint(ODEs_CA3,y0,t)
    plt.figure(1)
    plt.plot(t,y[:,1],label='Fa')
    plt.plot(t,y[:,2],label='Fb')
    plt.plot(t,y[:,3],label='Fc')
    plt.legend(loc='upper right')
    plt.xlabel('Volume(m^3)')
    plt.ylabel('molar flow rate(mol/s)')
    plt.show()
    plt.figure(2)
    plt.clf()
    plt.plot(t,y[:,0])
    plt.xlabel('Volume(m^3)')
    plt.ylabel('time(s)')
    plt.show()

