function param_estim_4lump

xdata = [1/60 1/30 1/20 1/10]; %time
ydata = [.5074 .3796 .2882 .1762; .3767 .4385 .4865 .5416; .0885 .136 .1681 .2108; .0274 .0459 .0572 .0714];
%%
% $\frac{dy_1}{dt}=-(k_{12}+k_{13}+k_{14}y_{1}^{2}$
%%
% $\frac{dy_2}{dt}=k_{12}y_{1}^{2}-k_{23}y_{2}-k_{24}y_{2}$
%%
% $\frac{dy_3}{dt}=k_{13}y_{1}^{2}-k_{23}y_{2}$
%%
% $\frac{dy_4}{dt}=k_{14}y_{1}^{2}-k_{24}y_{2}$
k(1)=1;
k(2)=.5;
k(3)=.3;
k(4)=0;
k(5)=.1;
params_guess = k;

x0(1) = 0.7;
x0(2) = .2;
x0(3)=.04;
x0(4)=.01;


[params,resnorm,residuals,exitflag,] = lsqcurvefit(@(params,xdata) ...
    ODEmodel2(params,xdata,x0),params_guess,xdata,ydata, [], []);


hold on
plot(xdata,ydata(1,:),'rx');
xlabel('t');
ylabel('x');
plot(xdata,ydata(1,:),'r+');
plot(xdata,ydata(2,:),'b+');
plot(xdata,ydata(3,:),'g^');
plot(xdata,ydata(4,:),'yo');
y_calc = ODEmodel2(params,xdata,x0);
plot(xdata,y_calc(1,:),'r-o');
plot(xdata,y_calc(2,:),'b-d');
plot(xdata,y_calc(3,:),'g-d');
plot(xdata,y_calc(4,:),'y-d');
hold off 
legend('x_1 data','x_2 data','x_1 fit','x_2 fit','Location','east');
end 
function dxdt = ODE2(t,x,params)

    k_12= params(1);
    k_13 = params(2);
    k_14=params(3);
    k_23=params(4);
    k_24=params(5);
    dxdt(1) = -(k_12+k_13+k_14)*x(1)^2;
    dxdt(2) = k_12*x(1)^2-k_23*x(2)-k_24*x(2);
    dxdt(3)=k_13*x(1)^2+k_23*x(2);
    dxdt(4)=k_14*x(1)^2+k_24*x(2);
    dxdt = dxdt';
end

function y_output = ODEmodel2(params,xdata,x0)

    for i = 1:length(xdata);

        tspan = [0:0.001:xdata(i)];

        [~,y_calc] = ode23s(@(t,x) ODE2(t,x,params),tspan,x0);

        y_output(i,:)=y_calc(end,:);

    end

    y_output = y_output';

end
