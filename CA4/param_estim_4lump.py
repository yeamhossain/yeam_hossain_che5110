# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 21:06:46 2017

@author: hossa
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 20:56:10 2017

@author: hossa
"""
import numpy as np

from scipy.optimize import curve_fit

from scipy.integrate import odeint

import matplotlib.pyplot as plt


timedata = np.array( [1/60, 1/30, 1/20, 1/10] ) # time

ydata = np.array( [[.5074,.3796,.2882,.1762], [.3767,.4385,.4865,.5416], [.0885,.136,.1681,.2108],[.0274,.0459,.0572,.0714]] )

params_guess = np.array([1,0.5,0.3,0,0.1])

y0=np.array([.7,.2,.04,.01])
def param_estim_4lump(y,t,args):

    dydt=[]
    k_12 = args[0]
    k_13 = args[1]
    k_14= args[2]
    k_23=args[3]
    k_24=args[4]
    dydt.append(-(k_12+k_13+k_14)*y[0]**2)
    dydt.append(k_12*y[0]**2-k_23*y[1]-k_24*y[1])
    dydt.append(k_13*y[0]**2+k_23*y[1])
    dydt.append(k_14*y[0]**2+k_24*y[1])
    dydt = np.transpose(dydt)
    dydt=  np.array(dydt)
    
    return dydt.ravel()

def ODEmodel1(timedata,*params):

    y0=np.array([.7,.2,.04,.01])
    y_output=[]

    for i in np.arange(len(timedata)):
        t_inc = 0.01
        tspan = np.arange(0,timedata[i]+t_inc,t_inc)
        y_calc = odeint(param_estim_4lump,y0,tspan,args=(params,))
        y_output.append(y_calc[-1,:])
    y_output=np.transpose(y_output)
    y_output=np.array(y_output)      
    return y_output.ravel()

params_output, pcov = curve_fit(ODEmodel1,timedata,ydata.ravel(),p0=params_guess)
y0=np.array([.7,.2,.04,.01])
y_calculatedguess = odeint(param_estim_4lump,y0,timedata,args=(params_guess,))
y_calculated = odeint(param_estim_4lump,y0,timedata,args=(params_output,))
print(y_calculated[:,0])
plt.plot(conversion, y_calculated[:,0], label='VGO data')
plt.plot(conversion, y_calculated[:,1], label='Gasoline data')
plt.plot(conversion, y_calculated[:,2], label='Gas and Coke data')
plt.plot(conversion, y_calculated[:,3])