function param_estim_3lump

xdata = [1/60 1/30 1/20 1/10]; %time
ydata = [.5074 .3796 .2882 .1762; .3767 .4385 .4865 .5416; .1159 .1819 .2253 .2822];
%%
% $\frac{dy_1}{dt}=-(k_1+k_3)y_{1}^{2}$
%%
% $\frac{dy_2}{dt}=k_1y_{1}^{2}-k_2y_2$
%%
% $\frac{dy_3}{dt}=k_3y_{1}^{2}+k_2y_2$
%initial guess
k(1) = 1;
k(2) = 0.5;
k(3)=0.5;
params_guess = k;

%initial conditions
x0(1)=1;
x0(2)=0;
x0(3)=0;

[params,residuals] = lsqcurvefit(@(params,xdata) ...
    ODEmodel1(params,xdata,x0),params_guess,xdata,ydata, [], []);


k_1=params(1);
k_2=params(2);
k_3=params(3);
plot(xdata,ydata,'x');
xlabel('t');
ylabel('x');
hold on
plot(xdata,ODEmodel1(params,xdata,x0),'-o');
hold off

function dxdt = ODE1_definition(t,x,params)

    k_1 = params(1);
    k_2 = params(2);
    k_3=params(3);
    dxdt(1)=-(k_1+k_3)*x(1)^2;
    dxdt(2)=k_1*x(1)^2-k_2*x(2);
    dxdt(3)=k_3*x(1)^2+k_2*x(2);
    dxdt=dxdt';

end

function y_output = ODEmodel1(params,xdata,x0)
    for i = 1:length(xdata);
        tspan = [0:0.01:xdata(i)];
        [~,y_calc] = ode23s(@(t,x) ODE1_definition(t,x,params),tspan,x0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output=y_output';
end

end 