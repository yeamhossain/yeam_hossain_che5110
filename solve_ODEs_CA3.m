function Y=solve_ODEs_CA3()
%a system of ODEs describes the molar flow artes of species A,B and C in
%mol/s
%the reactions are steady state but vary spatially along the volume of the
%reactor
%'V' is the independent variable
%T=temperature; FA,FB,FC=molar flow rate in mol/s

%initial conditions
FA=100;
FB=0;
FC=0;
T=423;

V_span=[0:.01:1]; % volume span
initial=[FA FB FC T];


function P=ODEs_CA3(V,X)% two arguments input
    
FA=X(1);
FB=X(2);
FC=X(3);
T=X(4);
FT=X(1)+X(2)+X(3);

%constant parameters
E1R=4000;%unit in kelvin 'k'
E2R=9000;%unit in kelvin 'k'
dHRx1A=-20000;%unit is J/(mol of A reacted in reaction 1)
dHRx2A=-60000;%unit is J/(mol of A reacted in reaction 2)
CpA=90;% J/mol.C  % C=celcius
CpB=90;% J/mol.C
CpC=180.0;% J/mol.C
Ua=4000.0;%J/m3.s.C
Ta=373.0;%J/m3.s.C
CT0=0.1;
T0=423; % unit in 'k'
    
    %%
    % $k_{1A}=10exp[\frac{E_1}{R}(\frac{1}{300}-\frac{1}{T})]$
k1A=10*exp(E1R*(1/300-1/T)); %unit in 1/s
%%
% $k_{2A}=0.09exp[\frac{E_2}{R}(\frac{1}{300}-\frac{1}{T})]$
k2A=0.09*exp(E2R*(1/300-1/T)); %unit in 1/s
%%
% $C_A=C_{T_0}(\frac{F_A}{F_T})(\frac{T_0}{T})$
%%
% $C_B=C_{T_0}(\frac{F_B}{F_T})(\frac{T_0}{T})$
%%
% $C_C=C_{T_0}(\frac{F_C}{F_T})(\frac{T_0}{T})$
CA=CT0*(FA/FT)*(T0/T);
CB=CT0*(FB/FT)*(T0/T);
CC=CT0*(FC/FT)*(T0/T);
%rate laws
%%
% $r_{1A}=-k_{1A}C_A$
%%
% $r_{2A}=-k_{2A}C^{2}_{A}$
r1A=-k1A*CA;
r2A=-k2A*CA^2;
%relative rates
%%
% $r_{1B}=k_{1A}C_A$
%%
% $r_{2C}=\frac{k_{2A}}{2}C^{2}_{A}$
r1B=-r1A;
r2C=-0.5*r2A;
%net rates
%%
% $r_A=r_{1A}+r_{2A}$
%%
% $r_B=r_{1B}$
%%
% $r_C=r_{2C}$
rA=r1A+r2A;
rB=r1B;
rC=r2C;
%mole balance
%%
% $\frac{dF_A}{dV}=r_A$
%%
% $\frac{dF_B}{dV}=r_B$
%%
% $\frac{dF_C}{dV}=r_C$
dFAdV=rA;
dFBdV=rB;
dFCdV=rC;
%energy balance
%%
    % $\frac{dT}{dV}=\frac{U_a(T_a-T)+(-r_1A)(-\Delta H_{Rx1A})+(-r_2A)(-\Delta H_{Rx2A})}{F_AC_{P_A}+F_BC_{P_B}+F_CC_{P_C}}$
dTdV=(Ua*(Ta-T)+(-r1A)*(-dHRx1A)+(-r2A)*(-dHRx2A))/(FA*CpA+FB*CpB+FC*CpC);


P=[dFAdV; dFBdV; dFCdV; dTdV];
end

[A,B]=ode45(@ODEs_CA3,V_span,initial);%differential equations call
subplot(1,2,2)
plot(A,B(:,1),A,B(:,2),A,B(:,3))
xlabel('volume(m^3)');
ylabel('molar flow rate(mol/s)');
legend('FA','FB','FC');
subplot(1,2,1)
plot(A,B(:,4))
xlabel('volume(m^3)');
ylabel('time(s)');
snapnow
end 